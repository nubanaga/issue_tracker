# SETUP

After setting up the Django server with "runserver" you may access the admin
page(ex:http://localhost:8000/admin) if you wish to add\edit\remove the current tickets for testing.
To test the frontend you have to go to the list page (ex: localhost:8000/tracker/list/).

CREDENTIALS:
user:admin
pass:admin123


# SHORT DESCRIPTION
Once on the list page you are presented with the list of the current tickets separated by tabs according
to their state (open, pending,closed).
Once you click a ticket you are taken to the detail page of the ticket where you can change the state.
The way to work with the tracking system is pretty straightforward and simple.

I could've done pretty much everything with Django but I tried to get as much as JS as
I reasonably could. The JS code is pretty straightforward and simple as well, that's why I didn't feel
the need to document the code a lot.

# DEVELOPMENT PROCESS
After setting up the Django server(defining static files directories, template directories,etc) the
model Issue was added with the 3 requested attributes.

The model was added to the admin.py file for the purpose easily populating the DB with test data.

The next step was creating the urls and the respective views for the webpages.

VIEWS:
    -list: Gets issues separated by state and sends them to the template serialized as json for easy reading
    using js.
    -issue: Gets the issue depending on the id passed in the request. If the request method is POST, then it
    checks if the field with the new state is present in the form, if it is then it checks if the new state
    is higher than the current(0-open, 1-pending, 2-closed) just to guarantee there is no manipulation.


#POSSIBLE IMPROVEMENTS

-Login system
-Better UI
-Events timestamps(when the issue was opened, closed,etc)
-Issue ordering in the list view
-Others...


