from django.core import serializers
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from issue.models import Issue, STATE_CLOSED, STATE_PENDING, STATE_OPEN


def list(request):
    ctx = {
        "open_issues": serializers.serialize('json', Issue.objects.filter(state=STATE_OPEN)),
        "pending_issues": serializers.serialize('json', Issue.objects.filter(state=STATE_PENDING)),
        "closed_issues": serializers.serialize('json', Issue.objects.filter(state=STATE_CLOSED)),
    }
    return render(request, "list.html", context=ctx)


@csrf_exempt
def issue(request, pk):
    issues = Issue.objects.filter(pk=pk)
    if request.method == "POST":
        if "next-state" in request.POST:

            issue = issues.first()
            new_state = int(request.POST["next-state"])

            if new_state > issue.state:
                issue.state = new_state
                issue.save()

    ctx = {"issue": serializers.serialize('json', issues)}
    return render(request, "issue.html", context=ctx)
