from django.db import models

TICKET_STATE_CHOICES = [
    (0, "OPEN"),
    (1, "PENDING"),
    (2, "CLOSED"),
]

STATE_OPEN = TICKET_STATE_CHOICES[0][0]
STATE_PENDING = TICKET_STATE_CHOICES[1][0]
STATE_CLOSED = TICKET_STATE_CHOICES[2][0]


class Issue(models.Model):
    state = models.IntegerField(choices=TICKET_STATE_CHOICES)
    title = models.CharField(max_length=150)
    description = models.TextField()

    def __str__(self):
        return self.title
