from django.urls import path

from issue import views

urlpatterns = [
    path('list/', views.list, name='list'),
    path('issue/<int:pk>', views.issue, name='issue'),
]
