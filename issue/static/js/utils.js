function changeTab(evt) {
    var i, tabcontent, tablinks;

    //Hide all tabs
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    //Reset styles
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    //Apply style to active tab
    document.getElementById("tab_" + evt.target.id).style.display = "block";
    evt.currentTarget.className += " active";
}

function populateList(el, data) {
    var list = el.children[0];
    for (let i in data) {
        let ticket, pk, title, text, line;

        ticket = data[i].fields;
        pk = data[i].pk;
        title = ticket.title;

        text = document.createTextNode(title);
        line = document.createElement("div");
        line.appendChild(text);

        line.classList.add("line");
        list.appendChild(line);

        line.onclick = function () {
            window.location.href = '/tracker/issue/' + pk;
        }
    }
}
