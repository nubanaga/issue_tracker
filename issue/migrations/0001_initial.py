# Generated by Django 3.0.2 on 2020-01-18 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.IntegerField(choices=[(0, 'CLOSED'), (1, 'PENDING'), (2, 'OPEN')])),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField()),
            ],
        ),
    ]
